h, j, k, l .. move left, down, up, right
w, b, e .. move word at a time

0, $ .. move to start/end of line
gg, G .. move to start / end of buffer

f or F then [char] .. move to next/prev given char in line
; and , .. repeat last f or F

% .. Goto corresponding parentheses

/[text] and then n, N .. Search text


[n][action/movement] .. do n times, e.g. 3w


i, I .. change to insert mode

d[movement] .. cut by giving movement
D .. cut to end of line
S .. clear current line; to insert mode

x, X .. remove a character
r[char] .. replaces character below cursor
a, A .. append after this character / line
o, O .. add new line

ci[movement] .. change inside of given movement


yy .. copy current line
p .. Paste copied text after cursor.
