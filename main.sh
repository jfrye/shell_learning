#!/usr/bin/env bash

echo \
"Welcome to the terminal guide ;)
I'm gonna guide you through some shtuff!

Please choose a topic:"

ls ./commands/ | sed -r 's/(.*)\.sh/ - \1/'

echo "\n(type one of the above options)"

read topic

sh ./commands/${topic}.sh