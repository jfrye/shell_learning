#!/usr/bin/env bash

echo "\n---HARDWARE---"

echo "\ndf --- display disk usage"
df

echo "\nfree --- display memory usage"
free