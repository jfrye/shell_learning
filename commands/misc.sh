#!/usr/bin/env bash

echo "\n---MISC---"

echo "\ndate --- displays current time and date:"
date

echo "\ncal --- displays a calendar of the current month"
cal
