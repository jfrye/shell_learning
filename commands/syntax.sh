#!/usr/bin/env bash

echo \
"Before discussing built-in commands, lets discuss the basic syntax of the terminal.

Please select a topic:

1 - variables
2 - flow control
"

read topic

echo ${topic}
